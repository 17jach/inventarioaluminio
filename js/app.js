let url = "../inventarioaluminio_back/";



const pruebaBoton =() =>{
    alert("Hola");
}

const agregarMaterial = () => {


    let nombreMat = document.getElementById("nombreMaterial").value;
    let colorMat = document.getElementById("colorMaterial").value;
    let precioMat = document.getElementById("precioMaterial").value;


    parametros = {
        "nombreMat": nombreMat,
        "colorMat": colorMat,
        "precioMat": precioMat
    }
    fetch(url + "postMaterial.php", {
        method: 'POST',
        body: JSON.stringify(parametros), // data can be `string` or {object}!
        headers: {
            'Content-Type': 'application/json'
        }
    })

        .then(res => {
            console.log(res.url);
        });
}

const obtenerTodo = () => {

    fetch(url + "getAll.php", {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(res => {
            console.log(res);
            let materiales = res;

            let color;
            let id;
            let nombre;
            let precio;
            let divContenedor = document.getElementById("divContenedor");
            let divContent;




            materiales.forEach(element => {
                color = element.colorMat;
                id = element.idMaterial;
                nombre = element.nombreMat;
                precio = element.precioMat;

                divContent = document.createElement(`div`);
                divContent.classList.add('divCard');
                divContent.innerHTML = `
                <div id="divId">
                        <h3>ID del material</h3>
                        <p id="idMaterial" > `+ id + `</p>
                </div>
                <div id="divNombre">
                    <h3>Nombre del material</h3>
                    <p id="nombreMaterial"> `+ nombre + `</p>
                </div>
                <div id="divColor">
                    <h3>Color del material</h3>
                    <p id="colorMaterial"> `+ color + `</p>
                </div>
                <div id="divPrecio">
                    <h3>Precio del material</h3>
                    <p id="precioMaterial"> `+ precio + `</p>
                </div>
            `;

                divContenedor.appendChild(divContent);

            });
        })
}


const obtenerInfo = () => {

    var busca = document.getElementById("buscaMaterial").value;
    fetch(url + "getInfo.php?busca=" + busca, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(res => {

            let materiales = res;
            let nombre, color, precio;
            let divContenedor = document.getElementById("divContenedor");
            let divContent;


            materiales.forEach(element => {
                nombre = element.nombreMat;
                color = element.colorMat;
                precio = element.precioMat;
                console.log("**********");
                console.log("Nombre: " + nombre);
                console.log("Color: " + color);
                console.log("Precio: " + precio);

                divContent = document.createElement(`div`);
                divContent.classList.add('divCard');
                divContent.innerHTML = `
                
                <div id="divNombre">
                    <h3>Nombre del material</h3>
                    <p id="nombreMaterial"> `+ nombre + `</p>
                </div>
                <div id="divColor">
                    <h3>Color del material</h3>
                    <p id="colorMaterial"> `+ color + `</p>
                </div>
                <div id="divPrecio">
                    <h3>Precio del material</h3>
                    <p id="precioMaterial"> `+ precio + `</p>
                </div>
            `;

                divContenedor.appendChild(divContent);


            });

        })
}